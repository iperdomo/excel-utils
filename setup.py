from setuptools import setup

setup(name='excel_utils',
      version='0.0.1',
      description='Testing',
      author='Eduardo El Grande',
      author_email='eduardo@bitcoin.com',
      license='MPL-2.0',
      url='https://gitlab.com/iperdomo/excel-utils.git')